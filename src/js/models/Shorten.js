import axios from 'axios';

export default class Shorten {
  constructor(link) {
    this.link = link;
  }

  async getShortenedLink() {
    try {
      const response = await axios.post('https://rel.ink/api/links/', {
        url: this.link,
      });
      //console.log(response);
      this.result = response.data;
      this.url = response.data.url;
      this.id = response.data.hashid;
      // console.log(this.result);
      // console.log(this.url);
      // console.log(this.id);
    } catch (error) {
      alert(error);
    }
  }
}

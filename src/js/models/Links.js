export default class Links {
  constructor() {
    this.links = [];
  }

  addLink(hashid, url) {
    const link = { hashid, url };
    this.links.push(link);
    //console.log(link);

    this.persistData();
    return link;
  }

  persistData() {
    localStorage.setItem('links', JSON.stringify(this.links));
  }

  readStorage() {
    const storage = JSON.parse(localStorage.getItem('links'));
    if (storage) {
      this.links = storage;
    }
  }
}

import '../styles/main.scss';
import * as menuView from './views/menuView';
import * as shortenView from './views/shortenView';
import * as linksView from './views/linksView';
import { elements } from './views/base';
import Shorten from './models/Shorten';
import Links from './models/Links';
import isUrlValid from 'url-validation';

/** Global state of the app
 */

const state = {};

/**
 * MENU CONTROLLER
 */

elements.menuBtn.addEventListener('click', () => {
  // Listen to media query change
  const mediaQuery = window.matchMedia('(min-width: 900px)');
  mediaQuery.addListener(menuView.toggleMenu);

  // Toggle menu
  menuView.toggleMenu(mediaQuery);
});

/**
 * SHORTEN CONTROLLER
 */

const controlShorten = async () => {
  // Get link from view
  const link = shortenView.getInput();
  // Validate URL
  const valid = isUrlValid(link);

  if (link && valid) {
    //Create new Shorten object and add to state
    state.shorten = new Shorten(link);
    console.log(state.shorten);

    // Prepare UI for result (clear the input)
    shortenView.clearInput();

    try {
      // Shorten link from the input
      await state.shorten.getShortenedLink();

      // Render result on UI
      shortenView.renderResult(state.shorten.result);

      //Add link to localStorage
      controlLink();
    } catch (error) {
      alert('Something went wrong');
    }
  } else if (!link) {
    // Dispaly error if the input field is empty
    shortenView.displayErrorMsg();

    // Remove error message
    setTimeout(() => {
      shortenView.removeErrorMsg();
    }, 1500);
  } else if (!valid) {
    alert('Enter a valid URL!');
    shortenView.clearInput();
  }
};

// Handle form submit event
elements.shortenForm.addEventListener('submit', (event) => {
  event.preventDefault();
  controlShorten();
});

// Handle Copy button click event (copy to clipboard)
elements.resultsList.addEventListener('click', (event) => {
  // Get Copy button and Shortened link
  const copyBtn = event.target.closest('.copy-btn');
  const shortLink = event.target.parentNode.childNodes[1];
  //console.log(copyBtn);
  //console.log(shortLink);

  if (copyBtn) {
    // Copy to clipboard
    linksView.copyToClipboard(shortLink);

    // Toggle button
    linksView.toggleCopyBtn(copyBtn);
  }
});

/**
 * LINK CONTROLLER
 */

const controlLink = () => {
  if (!state.links) {
    state.links = new Links();
  }

  state.links.addLink(state.shorten.result.hashid, state.shorten.result.url);

  // console.log(newLink);
  // console.log(state.links);
};

// Restore shortened links on page load
window.addEventListener('load', () => {
  state.links = new Links();
  //console.log(state.links);

  state.links.readStorage();
  state.links.links.forEach((link) => linksView.renderLink(link));
});

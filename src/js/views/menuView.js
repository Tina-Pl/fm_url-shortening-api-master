import { elements } from './base';

export const toggleMenu = (mediaQuery) => {
  if(mediaQuery.matches) {
    elements.menu.style.display = 'flex';
  } else {
    const isVisible = elements.menu.style.display === 'flex';
    //console.log(notVisible);
    elements.menu.style.display = isVisible ? 'none' : 'flex';
  }
};

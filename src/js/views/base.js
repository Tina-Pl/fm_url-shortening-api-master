export const elements = {
  menu: document.querySelector('.nav-navbar'),
  menuBtn: document.querySelector('.menu-icon'),
  shortenForm: document.querySelector('.shorten'),
  shortenInput: document.querySelector('.shorten-link-field'),
  shortenBtn: document.querySelector('.shorten-btn'),
  errorMsg: document.querySelector('.error'),
  resultsList: document.querySelector('.results-list')
};
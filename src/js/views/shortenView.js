import { elements } from './base';

export const getInput = () => elements.shortenInput.value;

export const clearInput = () => elements.shortenInput.value = '';

export const displayErrorMsg = () => {
  const markup = '<span class="error-msg">Please add a link</span>';
  elements.errorMsg.insertAdjacentHTML('beforeend', markup);
  elements.shortenInput.classList.add('field-error');
  elements.shortenBtn.disabled = true;
};

export const removeErrorMsg = () => {
  elements.errorMsg.innerHTML = '&nbsp;';
  elements.shortenInput.classList.remove('field-error');
  elements.shortenBtn.disabled = false;
};

export const limitLinkLength = (link, limit=56) => {
  const newLink = [];

  if (link.length > limit) {
    link.split('').slice(0, limit).map(char => {
      newLink.push(char);
    })
    return `${newLink.join('')} ...`;
  }
  return link;
};

export const renderResult = (result) => {
  const markup = `
    <li class="results-link">
      <div class="results-link-full">${limitLinkLength(result.url)}</div>
      <div class="results-link-display">
        <div class="results-link-shortened">https://rel.ink/${result.hashid}</div>
        <input class="btn copy-btn btn-squared" type="button" value="Copy">
      </div>
    </li>
  `;

  elements.resultsList.insertAdjacentHTML('beforeend', markup);
};




import { elements } from './base';
import { limitLinkLength } from './shortenView';

export const copyToClipboard = (link) => {
  // Copy link to clipboard
  const txtEl = document.createElement('textarea');
  txtEl.value = link.textContent;
  document.body.appendChild(txtEl);
  txtEl.select();
  document.execCommand('copy');
  txtEl.remove();
};

export const renderLink = (link) => {
  const markup = `
  <li class="results-link">
    <div class="results-link-full">${limitLinkLength(link.url)}</div>
    <div class="results-link-display">
      <div class="results-link-shortened">https://rel.ink/${link.hashid}</div>
      <input class="btn copy-btn btn-squared" type="button" value="Copy">
    </div>
  </li>
`;

  elements.resultsList.insertAdjacentHTML('beforeend', markup);
};

export const toggleCopyBtn = (btn) => {
  // Change Copy button
  btn.classList.add('copied');
  btn.value = 'Copied!';
};
